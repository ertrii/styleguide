import React, { Fragment } from 'react'
import PropTypes from 'prop-types'

import './Title.css'

export default function Title({ children, size }){
    return(<Fragment>
        { size === 'small' && <h3>{children}</h3> }
        { size === 'normal' && <h2>{children}</h2> }
        { size === 'large' && <h1>{children}</h1> }
    </Fragment>)
}

Title.propTypes = {
    children: PropTypes.node.isRequired,
    /**
     * Size title
     */
    size: PropTypes.string.isRequired
}