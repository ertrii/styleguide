import styled from 'styled-components'

export default styled.input`
    border: 1px solid #f1f1f1;
    padding: 8px;
`