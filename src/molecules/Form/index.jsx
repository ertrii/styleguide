import React from 'react'
import PropTypes from 'prop-types'
import FormStyled from './Form.styled'

const Form = ({ children }) => {
    return (<FormStyled>
        {children}
    </FormStyled>)
}

Form.propTypes = {
    /**
     * Recibe componentes
     */
    children: PropTypes.node.isRequired
}

export default Form