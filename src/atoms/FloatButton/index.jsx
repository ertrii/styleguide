import React from 'react'
import PropTypes from 'prop-types'

import FloatButtonStyled from './styled'

export default function FloatButton({ children }){
    return (
        <FloatButtonStyled>
            { children }
        </FloatButtonStyled>
    )
}

FloatButton.propTypes = {
    children: PropTypes.node.isRequired
}