import React from 'react'
import PropTypes from 'prop-types'

import LabelStyled from './Label.styled'

export default function Label({text, ...props}){
    return(
        <LabelStyled {...props} >{text}</LabelStyled>
    )
}

Label.propTypes = {
    text: PropTypes.string.isRequired
}