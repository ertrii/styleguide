import styled from 'styled-components'

export default styled.label`
    font-family: sans-serif;
    font-size: .8em;
    margin-top: 1.2em;
    margin-bottom: .5em;
    color: #9d9d9d;
`