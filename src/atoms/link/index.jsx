import React from 'react'
import PropTypes from 'prop-types'

import { StyledLink } from './style'
import './index.css'

export default function Link({ children, path }) {
    return (<StyledLink href={path} >{children}</StyledLink>)
}

Link.propTypes = {
    children: PropTypes.node.isRequired,
    /**
     * Url
     */
    path: PropTypes.string.isRequired
}