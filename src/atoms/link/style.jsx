import styled from 'styled-components'

export const StyledLink = styled.a`
  text-decoration: none;
  font-weight: bold;
  color: steelblue;
  text-transform: uppercase;
  font-family: sans-serif;
  &:hover{
    text-decoration: underline;
  }
`
