import styled from 'styled-components'

export default styled.div`
    width: 50px;
    height: 50px;
    border-radius: 50%;
    background-color: steelblue;
    box-shadow: 1px 1px 10px -2px #444;
    display: flex;
    justify-content: center;
    align-items: center;
    color: white;
    cursor: pointer;
    transition: .1s ease;
    &:active{
        box-shadow: 0 0 0 0 #444;
    }
`