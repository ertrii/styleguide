module.exports = {
    sections: [
        {
          name: 'Atoms',
          components: 'src/atoms/**/*.{js,jsx}'
        },
        {
          name: 'Molecules',
          components: 'src/molecules/**/*.{js,jsx}',
        },
        {
          name: 'Organisms',
          components: 'src/organisms/**/*.{js,jsx}',
        },
        {
          name: 'Templates',
          components: 'src/templates/**/*.{js,jsx}',
        },
        {
          name: 'Pages',
          components: 'src/pages/**/*.{js,jsx}',
        },
    ],
    ignore: [
        'src/**/*.spec.js',
        'src/**/*.test.js',
        'src/**/*.{style,styled}.{js,jsx}',
        'src/**/{style,styled}.{js,jsx}'
    ]
}