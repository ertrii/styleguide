import styled from 'styled-components'

export default styled.form`
    max-width: 350px;
    border: 1px solid #f1f1f1;
    padding: 2em;
    box-sizing: border-box;
    display: flex;
    flex-direction: column;
`