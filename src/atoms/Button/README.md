
```jsx
const colors = ['', 'success', 'warning', 'danger']
initialState = { color: '' }
; // Render
<Button
    color={state.color}
    onClick={ ()=> setState({ color: colors[1]}) }>
    Click Me!
</Button>
```

```jsx
<Button color="success">Click Me</Button>
```

```jsx
const color = 'warning'
;
<Button color={color}>Click Me</Button>
```

```jsx
<Button color="danger">Click Me</Button>
```
