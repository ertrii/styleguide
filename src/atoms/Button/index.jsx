import React from 'react'
import PropTypes from 'prop-types'
import './index.css'

function Button({ color, children, ...props }) {
    return(<button className={`Button ${color}`} {...props} >{children}</button>)
}

Button.propTypes = {
    /**
     * Style color for the Button
     */
    color: PropTypes.oneOf(['success', 'warning', 'danger']),
    children: PropTypes.node.isRequired
 }

Button.defaultProps = {
    color: ''
}

export default Button
