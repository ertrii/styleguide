```jsx
import Title from '../../atoms/Title'
import TextField from '../../atoms/TextField';
import Label from '../../atoms/Label';
import Button from '../../atoms/Button';
<Form>
    <Title size="large">Formulario</Title>
    <Label text="Usuario" />
    <TextField />
    <Label text="Contraseña" />
    <TextField type="password" />
    <Button color="success" style={{ marginTop: '1.5em' }} >INICIAR</Button>
</Form>
```
